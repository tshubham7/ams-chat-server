package server

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/ams-chat-server/service"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

// avoid looping to all connections
// access the connections by user id
var clients = make(map[string]*Client) // connected clients

// Client with some details
type Client struct {
	ID   string
	Conn *websocket.Conn
}

var broadcast = make(chan Message) // broadcast channels

// Configure the upgrader
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// Message struct
type Message = service.Message

// HandleWebsocketConnections is the handler function for websocket connections
func HandleWebsocketConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}
	// Make sure we close the connection when the function returns
	defer ws.Close()

	userID := r.URL.Query().Get("userID")
	roomID := r.URL.Query().Get("chatID")
	var client *Client
	var ok bool
	if userID == "" {
		log.Println("client connected without user id")
		// return
	} else {
		// updating our old client with new connection
		_, ok = clients[userID]
		if ok {
			client = clients[userID]
			client.Conn = ws
		} else {
			client = &Client{ID: userID, Conn: ws}
			clients[client.ID] = client
		}
	}

	err = CreateRoom(roomID)
	if err != nil {
		ws.WriteJSON(
			fmt.Sprintf(`"message": "error encountered while fetching the chat/room", "error": "%s"`, err))
		ws.Close()
	}
	log.Println("client connected", client.ID)

	client.Conn.WriteJSON(service.Message{Text: "successfully connected client id: " + client.ID})

	for {
		var msg Message
		// Read in a new message as JSON and map it to a Message object
		err := ws.ReadJSON(&msg)
		if err != nil {
			log.Printf("error while reading msg: %v", err)
			log.Printf("disconnecting user: %v", msg.Sender)
			// delete(clients, client.ID)
			ws.Close()
			break
		}
		// Send the newly received message to the broadcast channel
		broadcast <- msg
	}
}

// HandleMessages looks all the time for any new message from client
func HandleMessages() {
	for {
		// Grab the next message from the broadcast channel
		msg := <-broadcast
		fmt.Println("message from user, ", msg)
		id, _ := uuid.NewRandom() // generate the uuid
		msg.ID = id.String()
		msg.CreatedAt = time.Now()          // timestamp
		notifyUsers := broadcastToRoom(msg) // sending msg to all clients in the rooms
		go handleForDB(msg, notifyUsers)    // update on database
	}
}

// update the message in the database according to the action
func handleForDB(msg Message, users map[string]bool) {
	switch msg.Action {
	case "create":
		service.NewMessageService().Create(msg, users)
	case "edit":
		service.NewMessageService().Edit(msg.ID, msg.Text)
	case "delete":
		service.NewMessageService().Delete(msg.ID)
	default:
		// do nothing
	}
}
