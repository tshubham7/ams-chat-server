package server

import (
	"log"
)

// send message to all clients in the room
func broadcastToRoom(msg Message) map[string]bool {
	// Send it out to every client that is currently connected
	room := chats[msg.ChatID]
	var client *Client
	var ok bool
	unreadMsgClients := make(map[string]bool)
	for c := range room.Users {
		client, ok = clients[room.Users[c]]
		unreadMsgClients[room.Users[c]] = ok
		if ok {
			err := client.Conn.WriteJSON(msg)
			if err != nil {
				unreadMsgClients[room.Users[c]] = false
				log.Printf("error while sending the msg: %v", err)
				client.Conn.Close()
				// delete(clients, client.ID)
			}
		}
	}
	return unreadMsgClients
}
