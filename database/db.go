package database

import (
	"fmt"
	"log"
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// DBWrite specifies the database
var DBWrite *sqlx.DB

// initializing the databse
func init() {

	writeEndpoint := os.Getenv("AMS_WRITE_END_POINT")
	writePort := os.Getenv("AMS_WRITE_PORT")
	user := os.Getenv("AMS_USER")
	dbName := os.Getenv("AMS_DBNAME")
	password := os.Getenv("AMS_PASSWORD")

	ssl := "require"

	var writeConnection = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		writeEndpoint, writePort, user, password, dbName, ssl,
	)

	var err error
	DBWrite, err = sqlx.Open("postgres", writeConnection)

	if err != nil {
		log.Println(err)
		log.Panic(err)
	}

	err = DBWrite.Ping()
	if err != nil {
		log.Println(err)
		log.Panic(err)
	}
}
