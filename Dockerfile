FROM golang:1.13

RUN mkdir /chat-service

ADD . /chat-service

WORKDIR /chat-service

RUN go build -o chat-service .

EXPOSE 3000
EXPOSE 443

CMD ["/chat-service/chat-service"]


