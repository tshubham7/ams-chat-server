package server

import "github.com/ams-chat-server/service"

// SyncConnections sync everything with database
func SyncConnections() {
	// SyncClients() // no need for this now
	// SyncRoom()  // no need for this now
	// fmt.Println("sync complete")
	go HandleMessages()
}

// SyncRoom sync all rooms from database
func SyncRoom() {
	chatList, _ := service.NewChatService().ListAll()
	var c Chat
	for r := range chatList {
		c.Name = chatList[r].Name
		c.ID = chatList[r].ID
		chats[c.ID] = &c

		members, _ := service.NewChatService().Users(chatList[r].ID)
		// Adding clients/members to the chat
		for m := range members {
			c.Users = append(c.Users, members[m].UserID)
			// c := clients[members[m].UserID]
			// if members[m].IsAdmin {
			// 	c.Admin = c
			// }
			// if c != nil {
			// 	c.Clients[c.ID] = c
			// }
		}
	}
}

// SyncClients sync all clients with the database
func SyncClients() {
	users, _ := service.NewChatService().AllUsers()
	// var client Client
	for u := range users {
		client := Client{ID: users[u].ID}
		// client.ID = users[u].ID
		// client.Conn = ws
		clients[client.ID] = &client
	}
}
