package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/ams-chat-server/server"
)

func main() {
	// Configure websocket route

	http.HandleFunc("/ws", server.HandleWebsocketConnections)
	http.HandleFunc("/", indexHandler)

	go server.HandleMessages()

	log.Println("listening on port :3000")
	err := http.ListenAndServe(":3000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "welcome to cocre8 chat service")
}
