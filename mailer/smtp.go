package mailer

import (
	"bytes"
	"fmt"
	"log"
	"net/smtp"
	"os"
	"strconv"
	"text/template"

	"gopkg.in/gomail.v2"
)

var fromEmail string
var password string
var smtpHost string
var smtpPort int
var auth smtp.Auth

func init() {
	fromEmail = os.Getenv("AMS_SMTP_EMAIL")
	password = os.Getenv("AMS_SMTP_PASSWORD")
	smtpHost = os.Getenv("AMS_SMTP_HOST")
	port := os.Getenv("AMS_SMTP_PORT")
	smtpPort, _ = strconv.Atoi(port)
	log.Println("smtp secret:", password, fromEmail, smtpHost, smtpPort)
	if smtpPort == 0 {
		panic("invalid port: " + port)
	}
	auth = smtp.PlainAuth("", fromEmail, password, smtpHost)
}

// Send Email using template
func (r *Request) Send() {
	fmt.Println("sending email to:", r.tos)
	err := r.parseTemplate(r.templateName, r.templateData)
	if err != nil {
		log.Println("failed to send email:", err)
		return
	}
	ok, _ := r.sendEmail()
	log.Println(ok)
}

//Request struct
type Request struct {
	tos          []string
	subject      string
	body         string
	templateName string
	templateData interface{}
}

func NewMail(tos []string, subject string, templateName string, templateData interface{}) *Request {
	return &Request{
		tos:          tos,
		subject:      subject,
		templateName: templateName,
		templateData: templateData,
	}
}

func (r *Request) sendEmail() (bool, error) {
	m := gomail.NewMessage()
	m.SetHeader("From", "AMS <"+fromEmail+">")
	m.SetHeader("To", r.tos...)
	m.SetHeader("Subject", r.subject)
	m.SetBody("text/html", r.body)

	d := gomail.NewPlainDialer(smtpHost, smtpPort, fromEmail, password)
	if err := d.DialAndSend(m); err != nil {
		log.Println("error while sending email", err)
		return false, err
	}
	return true, nil
}

func (r *Request) parseTemplate(templateFileName string, data interface{}) error {
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return err
	}
	r.body = buf.String()
	return nil
}
