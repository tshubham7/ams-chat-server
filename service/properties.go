package service

import (
	"time"

	"github.com/jmoiron/sqlx/types"
)

// Detail detail of a room for select query
type Detail struct {
	ID        string         `json:"id" db:"id"`
	Name      string         `json:"name" db:"name"`
	Type      string         `json:"type" db:"type"`
	Imagekey  string         `json:"Image" db:"image_key"`
	CreatedAt string         `json:"createdAt" db:"created"`
	Users     types.JSONText `json:"users" db:"users"`
}

// Chat list of chat/rooms for select query
type Chat struct {
	ID        string `json:"id" db:"id"`
	Name      string `json:"name" db:"name"`
	Type      string `json:"type" db:"type"`
	Imagekey  string `json:"Image" db:"image_key"`
	CreatedAt string `json:"createdAt" db:"created_at"`
}

// Members list of users joined the group/room
type Members struct {
	UserID string `json:"userId" db:"user_id"`
	// ID           string     `json:"id" db:"id"`
	// RoomID       string     `json:"roomId" db:"room_id"`
	// Name         string     `json:"name" db:"name"`
	// ProfileImage string     `json:"profileImage" db:"profile_image"`
	// Blocked      *time.Time `json:"blocked" db:"blocked"`
	IsAdmin bool `json:"isAdmin" db:"is_admin"`
	// Mute         bool       `json:"mute" db:"mute"`
	// JoinedDate   *time.Time `json:"joinedDate" db:"joined_date"`
}

// Message with some information about the actual message
type Message struct {
	ID        string    `json:"id" db:"id"`              // Message id
	Sender    string    `json:"senderId" db:"sender_id"` // sender user_id
	ChatID    string    `json:"chatId" db:"chat_id"`     // chat_id
	Text      string    `json:"text" db:"text"`          // actual message
	Action    string    `json:"action" db:"action"`      // should be any one of these: create, edit and delete
	CreatedAt time.Time `json:"created" db:"created"`
}

// User for select query
type User struct {
	ID   string `json:"id" db:"id"`
	Name string `json:"name" db:"name"`
}

// UserEmail ...
type UserEmail struct {
	ID    string `json:"id" db:"id"`
	Email string `json:"email" db:"email"`
	Name  string `json:"name" db:"name"`
}
