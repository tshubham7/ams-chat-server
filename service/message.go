package service

import (
	"fmt"
	"log"

	"github.com/ams-chat-server/database"
	"github.com/jmoiron/sqlx"
)

// MessageService is responsible for all services related to messages
type MessageService interface {
	Create(Message, map[string]bool) error
	Edit(string, string) error
	Delete(string) error
	Messages(string) ([]Message, error)
}

type messageStore struct {
	*sqlx.DB
}

// NewMessageService serves the services
func NewMessageService() MessageService {
	return &messageStore{database.DBWrite}
}

// create a message,
// takes message payload and the list of users with websocket connection status
func (st *messageStore) Create(msg Message, nusers map[string]bool) error {
	q := `INSERT INTO chat_message(id, sender_id, chat_id, text, created)
		VALUES(:id, :sender_id, :chat_id, :text, :created)`
	_, err := st.NamedExec(q, msg)
	if err != nil {
		log.Println("go routine error, error while creating message", err)
	}
	var usersListString string // contains comma separated user id
	for userID, connected := range nusers {
		if userID != msg.Sender {
			// go pushnotification.PushToUser(userID, msg.ID) // sending push
			// we will send email here
			if !connected {
				usersListString = "'" + userID + "', " // users for unread message counter
			}
		} // make sure we don't count for these sender
	} // sending push notification
	if usersListString != "" {
		usersListString = usersListString[:len(usersListString)-2]
		go st.updateMsgCounter(msg.ChatID, usersListString)
		go st.emailToRecipients(&msg, usersListString)
	}
	return err
}

// update unread message counter
func (st *messageStore) updateMsgCounter(chatID, users string) error {
	q := fmt.Sprintf(`UPDATE chat_member set unread_msg=unread_msg+1 
	WHERE chat_id=$1 AND user_id IN (%s)`, users)
	_, err := st.Exec(q, chatID)
	if err != nil {
		log.Println("goroutine error while increment unread message counter: ", err.Error())
	}
	return err
}

// delete a message
func (st *messageStore) Delete(id string) error {
	q := `DELETE FROM chat_message WHERE id=$1`
	_, err := st.Exec(q, id)
	if err != nil {
		log.Println(err)
	}
	return err
}

// update a message
// takes id of the message and the updated text
func (st *messageStore) Edit(id, t string) error {
	q := `UPDATE chat_message SET text=$2 WHERE id=$1`
	_, err := st.Exec(q, id, t)
	if err != nil {
		log.Println(err)
	}
	return err
}

// list all messages for the room
// takes room id
func (st *messageStore) Messages(RoomID string) ([]Message, error) {
	var m = []Message{}
	q := `SELECT id, sender_id, text, media_key, created FROM chat_message WHERE room_id=$1`
	err := st.Select(&m, q, RoomID)
	if err != nil {
		log.Println(err)
	}
	return m, err
}
