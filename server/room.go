package server

import (
	"encoding/json"
	"fmt"

	"github.com/ams-chat-server/service"
)

// Chat for group chatting
type Chat struct {
	Name  string   `json:"name"`
	ID    string   `json:"id"`
	Type  string   `json:"type"`
	Admin string   `json:"admin"`
	Users []string `json:"users"`
}

var chats = make(map[string]*Chat)

// CreateRoom adding a room in the websocket
func CreateRoom(chatID string) error {
	var chat Chat
	r, err := service.NewChatService().GetID(chatID)
	if err != nil {
		fmt.Println("error encountered while fetching the chat from db", err)
		return err
	}
	err = json.Unmarshal([]byte(r.Users.String()), &chat.Users)
	if err != nil {
		fmt.Println("error encountered:", err)
	}
	chat.ID = r.ID
	chat.Name = r.Name
	chats[chat.ID] = &chat
	fmt.Println("chat added...")
	return nil
}
