package service

import (
	"log"

	// "github.com/ams-chat-service/db"
	"github.com/ams-chat-server/database"
	"github.com/jmoiron/sqlx"
)

// ChatService is responsible for all services related to room
type ChatService interface {
	List(string) ([]Chat, error)
	GetID(string) (Detail, error)
	ListAll() ([]Chat, error)
	// Update(string) error
	AddUser(string, string) error
	RemoveUser(string, string) error
	Users(string) ([]Members, error)
	AllUsers() ([]User, error)
}

type chatStore struct {
	*sqlx.DB
}

// NewChatService serves the chat services
func NewChatService() ChatService {
	return &chatStore{database.DBWrite}
}

// GetID chat details
func (st *chatStore) GetID(id string) (Detail, error) {
	var grp Detail
	q := `SELECT c.id, c.name, c.type, c.image_key, c.created, 
		(SELECT array_to_json(Array(SELECT user_id FROM chat_member WHERE chat_id=c.id))) AS users 
		FROM chat AS c WHERE id=$1`
	err := st.Get(&grp, q, id)
	if err != nil {
		log.Println(err)
	}
	return grp, err
}

// add user to the group/chat
func (st *chatStore) AddUser(id, uid string) error {
	q := `INSERT INTO chat_member(user_id, chat_id) VALUES($1, $2)`
	_, err := st.Exec(q, uid, id)
	if err != nil {
		log.Println(err)
	}
	return err
}

// remove user from a group/chat
func (st *chatStore) RemoveUser(id, uid string) error {
	q := `DELETE FROM chat_member WHERE user_id=$1 AND chat_id=$2`
	_, err := st.Exec(q, uid, id)
	if err != nil {
		log.Println(err)
	}
	return err
}

// list groups/chats
func (st *chatStore) List(id string) ([]Chat, error) {
	var grp = []Chat{}
	q := `SELECT id, name FROM chat WHERE id IN (SELECT chat_id FROM chat_member WHERE user_id=$1)`
	err := st.Select(&grp, q, id)
	if err != nil {
		log.Println(err)
	}
	return grp, err
}

// list all groups/chats
func (st *chatStore) ListAll() ([]Chat, error) {
	var grp = []Chat{}
	q := `SELECT id, name FROM chat`
	err := st.Select(&grp, q)
	if err != nil {
		log.Println(err)
	}
	return grp, err
}

// list of group/chat members
func (st *chatStore) Users(id string) ([]Members, error) {
	var list = []Members{}
	q := `SELECT user_id, is_admin FROM chat_member WHERE chat_id=$1`
	err := st.Select(&list, q, id)
	if err != nil {
		log.Println(err)
	}
	return list, err
}

// list of all users
func (st *chatStore) AllUsers() ([]User, error) {
	var list = []User{}
	q := `SELECT id, name FROM cre_user`
	err := st.Select(&list, q)
	if err != nil {
		log.Println(err)
	}
	return list, err
}
