package service

import (
	"fmt"
	"log"
	"time"

	"github.com/ams-chat-server/common"
	"github.com/ams-chat-server/mailer"
)

// tracking of users, we won't send email for every message
var mailedUsers = map[string]time.Time{}

// send email to receivers
func (st *messageStore) emailToRecipients(msg *Message, users string) error {

	senders := st.getEmails("'" + msg.Sender + "'")
	if len(senders) == 0 {
		fmt.Println("goroutine error while sending email to receivers, sender not found")
		return nil
	}

	receivers := st.getEmails(users)
	var data struct {
		Name string
		URL  string
	}
	for _, r := range receivers {
		if !checkEligible(r.ID) {
			continue
		}
		data.Name = r.Name
		data.URL = common.BackendDomain + "?chatID=" + msg.ChatID
		m := mailer.NewMail(
			[]string{r.Email}, "Message Alert",
			"templates/email/msg.html", data)
		m.Send()
	}

	return nil
}

// get users email
func (st *messageStore) getEmails(userIDs string) []UserEmail {
	var users = []UserEmail{}
	q := fmt.Sprintf(`SELECT id, email, name FROM ams_user WHERE id IN (%s)`, userIDs)
	err := st.Select(&users, q)
	if err != nil {
		log.Println("goroutine error while fetching users email: ", err.Error())
	}
	return users
}

// check if user is eligible to receive email
// means we will send 1 email in 6 hrs time period
func checkEligible(userID string) bool {
	now := time.Now()
	t, ok := mailedUsers[userID]
	if !ok {
		mailedUsers[userID] = now
		return true
	}

	// else if present, check of last time he has received a mail, it should be more than 6 hours
	if t.Add(1*time.Minute).String() <= now.String() {
		mailedUsers[userID] = now
		return true
	}
	return false
}
